package edu.rit.cs;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;

import java.io.File;
import java.io.IOException;

/**
 * U.S. Census Diversity Index
 * Based on https://www2.census.gov/programs-surveys/popest/datasets/2010-2017/counties/asrh/cc-est2017-alldata.csv
 */
public class USCB_Spark
{
    public static final String OutputDirectory = "dataset/USCB-outputs";
    public static final String DatasetFile = "dataset/USCB.csv";

    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null)
            for (File file : allContents)
                deleteDirectory(file);
        return directoryToBeDeleted.delete();
    }

    public static void uscb(SparkSession spark) {
        // parse dataset file
        Dataset ds = spark.read()
                .option("header", "true")
                .option("sep", ",")
                .option("inferSchema", "true")
                .csv(DatasetFile);

        // encoders are created for Java beans
        Encoder<USCBPopulationStat> uscbEncoder = Encoders.bean(USCBPopulationStat.class);
        Dataset<USCBPopulationStat> ds1 = ds.as(uscbEncoder);

        // filter and sum data across multiple years for all year groups
        Dataset ds2 = ds1.filter("AGEGRP = 0")
                .select("STNAME", "CTYNAME",
                "WA_MALE", "WA_FEMALE", "BA_MALE", "BA_FEMALE", "IA_MALE", "IA_FEMALE", "AA_MALE", "AA_FEMALE", "NA_MALE", "NA_FEMALE", "TOM_MALE", "TOM_FEMALE")
                .groupBy("STNAME", "CTYNAME")
                .sum();

        // calculated the total number of people in each race by aggregating genders
        Dataset ds3 = ds2.select("STNAME", "CTYNAME", "STNAME", "CTYNAME",
                "sum(WA_MALE)", "sum(WA_FEMALE)", "sum(BA_MALE)", "sum(BA_FEMALE)", "sum(IA_MALE)", "sum(IA_FEMALE)",
                "sum(AA_MALE)", "sum(AA_FEMALE)", "sum(NA_MALE)", "sum(NA_FEMALE)", "sum(TOM_MALE)", "sum(TOM_FEMALE)")
                .withColumn("WA", ds2.col("sum(WA_MALE)").plus(ds2.col("sum(WA_FEMALE)")))
                .withColumn("BA", ds2.col("sum(BA_MALE)").plus(ds2.col("sum(BA_FEMALE)")))
                .withColumn("IA", ds2.col("sum(IA_MALE)").plus(ds2.col("sum(IA_FEMALE)")))
                .withColumn("AA", ds2.col("sum(AA_MALE)").plus(ds2.col("sum(AA_FEMALE)")))
                .withColumn("NA", ds2.col("sum(NA_MALE)").plus(ds2.col("sum(NA_FEMALE)")))
                .withColumn("TOM", ds2.col("sum(TOM_MALE)").plus(ds2.col("sum(TOM_FEMALE)")));

        // calculated the total number of people
        Dataset ds4 = ds3.select("STNAME", "CTYNAME", "WA", "BA", "IA", "AA", "NA", "TOM")
                .withColumn("TOTAL", ds3.col("WA")
                        .plus(ds3.col("BA"))
                        .plus(ds3.col("IA"))
                        .plus(ds3.col("AA"))
                        .plus(ds3.col("NA"))
                        .plus(ds3.col("TOM")));

        // calculated the total minus Ni for each race i
        Dataset ds5 = ds4.select("STNAME", "CTYNAME", "WA", "BA", "IA", "AA", "NA", "TOM", "TOTAL")
                .withColumn("TOTAL-WA", ds4.col("TOTAL").minus(ds4.col("WA")))
                .withColumn("TOTAL-BA", ds4.col("TOTAL").minus(ds4.col("BA")))
                .withColumn("TOTAL-IA", ds4.col("TOTAL").minus(ds4.col("IA")))
                .withColumn("TOTAL-AA", ds4.col("TOTAL").minus(ds4.col("AA")))
                .withColumn("TOTAL-NA", ds4.col("TOTAL").minus(ds4.col("NA")))
                .withColumn("TOTAL-TOM", ds4.col("TOTAL").minus(ds4.col("TOM")));

        //calculates the diversity index
        Dataset ds6 = ds5.select("STNAME", "CTYNAME", "WA", "BA", "IA", "AA", "NA", "TOM", "TOTAL", "TOTAL-WA",
                "TOTAL-BA", "TOTAL-IA", "TOTAL-AA", "TOTAL-NA", "TOTAL-TOM")
                .withColumn("DIVERSITY",
                        ds5.col("WA").multiply(ds5.col("TOTAL-WA"))
                        .plus(ds5.col("BA").multiply(ds5.col("TOTAL-BA")))
                        .plus(ds5.col("IA").multiply(ds5.col("TOTAL-IA")))
                        .plus(ds5.col("AA").multiply(ds5.col("TOTAL-AA")))
                        .plus(ds5.col("NA").multiply(ds5.col("TOTAL-NA")))
                        .plus(ds5.col("TOM").multiply(ds5.col("TOTAL-TOM")))
                        .divide(functions.pow(ds5.col("TOTAL"),2))).orderBy("STNAME","CTYNAME");
        Dataset ds7 = ds6.select("STNAME", "CTYNAME", "DIVERSITY");

        //deletes old directory and writes rdd to output file
        deleteDirectory(new File(OutputDirectory));
        ds7.toJavaRDD().repartition(1).saveAsTextFile(OutputDirectory);

        ds7.show();
    }

    public static void main( String[] args ) {
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[4]");
        sparkConf.setAppName("Diversity index");
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        uscb(spark);
        jsc.close();
        spark.close();

        // sum male and female populations for each race
        // 1. collect all data //
        // 2. sum male and female populations for each race //
        // 3. calculate the diversity index for each county //
        // 4. move dataset into an output file.
    }
}
